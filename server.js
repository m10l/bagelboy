
/**
 * Simple shitty Express server
 */

'use strict';

var express = require('express');
var app = express();

var port = process.env.PORT || 5000;
app.use( express.static( __dirname + '/public' ) );

app.listen( port, function(){
	console.log( "Express server listening on port: " + port );
});